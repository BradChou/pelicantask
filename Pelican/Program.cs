﻿using System;
using System.Net;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Drawing;
using System.Drawing.Text;
using System.Drawing.Imaging;
using System.Net;
using System.Net.Mail;

namespace Pelican
{
    public class Program
    {
        private static string LYName = @"捷川貨運何小姐";
        private static string LYCity = @"桃園市";
        private static string LYArea = @"大園區";
        private static string LYAddress = @"國際路一段55號";          //已改
        private static string LYTel = @"0984346261";

        private static string AWSConnectionString = @"Initial Catalog=JunfuReal;server=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433; Initial Catalog = JunFuReal; Persist Security Info = True; User ID = admin; Password =%Tgb^Yhn";
        private static string TcDeliveryRequest = @"tcDeliveryRequests";
        private static string toPelicanFormatDbTable = @"Pelican_post_format";
        private static string requestPelican = @"Pelican_request";
        private static string logPelican = @"Pelican_log";              //單號資料的log
        private static string logPelicanSchedule = @"Pelican_schedule_log";         //排程的log
        private static string intermodalTransportationCheckNumber = @"intermodal_transportation_checknumber";
        private static string pelicanStatusTable = @"Pelican_status";
        private static int failTimes = 3; //一筆單號失敗嘗試次數
        private static string JFCustomerCode = @"8362991401";         //正式客代
        private static string pathSendToPelicanTxt = @"D:\PelicanFTPFile\";               //輸出需要上傳檔案路徑
        private static string pathSendToPelicanTxtUploaded = @"D:\PelicanFTPFileUploaded\";//將已經上傳的檔案移至此
        private static DateTime scheduleStartDate = new DateTime(2021, 2, 21);
        private static int companyIdPelican = 1;//正式宅配通編號:1
        private static string logPelicanFailInsertIntoPelicanRequest = @"D:\PelicanFailRequest\failRequests.txt";

        private static string ftpUriUpload = @"ftp://p2ap3.e-can.com.tw/";          //
        private static string ftpUriUploadReturn = @"Pickup/";      //逆物流跟正物流路徑不同
        private static string ftpUriUploadRoundTripReturn = @"ReservedPickup/";     //來回件逆物流
        private static string ftpAccountUpload = @"webedi";     //正式ftp帳號
        private static string ftpPasswordUpload = @"0922430089";
        private static string ftpUriDownload = @"ftp://p2ap3.e-can.com.tw/8362991401FY/";          //抓取回覆檔位置
        private static string ftpAccountDownload = @"delivery";          //
        private static string ftpPasswordDownload = @"0923505100";          //

        private static string ftpReturnPath = @"D:\PelicanReturn\";           //宅配通回傳檔案路徑
        private static string ftpFileReturnRecord = @"D:\PelicanReturnInserted\";                  //宅配通回傳檔案寫入完畢傳到這裡

        private static string statusTable = @"Pelican_status";      //貨態對應表
        private static string scanLogDriverCode = @"PP003";         //掃scan log的司機工號

        private static string PelicanReturnContent = @"Pelican_return_content";     //回覆檔寫入table名字

        private static string exceptionLogFolder = @"D:\PelicanExceptionCatcher\";
        private static string exceptionLogFileName = @"exception.txt";

        private static int alertIfCheckNumberCountLessThanTypeD = 2000;
        private static int alertIfCheckNumberCountLessThanTypeR = 500;

        public static void Primary()
        {
            DateTime latestTime = CatchLatestSuccessTime();           //抓取上次成功執行時間

            Catch99InDB(latestTime);                   //從資料庫抓取轉聯運的單號，並寫入專門table

            FtpUploaderToPelican();

            FtpCatchFromPelican();        //

            ReadReturnFile();           //讀取並寫入scanLog
        }
        private static DateTime CatchLatestSuccessTime()
        {
            int insertId = WriteDbLog(0, false, false);
            bool isSuccess = true;
            DateTime result = scheduleStartDate;

            string cmdText = string.Format(@"select max(start_time)'latest_success_time' from {0} with(nolock) where is_success = 1 and step = 1", logPelicanSchedule);
            using (SqlCommand cmd = new SqlCommand(cmdText))
            {
                cmd.Connection = new SqlConnection(AWSConnectionString);
                cmd.Connection.Open();
                try
                {
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            if (dr["latest_success_time"].ToString() != "")
                                result = DateTime.Parse(dr["latest_success_time"].ToString());
                        }
                    }
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    ExceptionRecord(0, e);
                }
                cmd.Connection.Close();
            }

            WriteDbLog(0, isSuccess, true, insertId);

            if (DateTime.Now.AddDays(-3) > result)          //避免為了一個錯誤單延誤整個程式
                result = DateTime.Now.AddDays(-3);

            return result;
        }
        public static void Catch99InDB(DateTime latestSuccessTime)
        {
            int insertId = WriteDbLog(1, false, false);
            bool isSuccess = true;

            string cmdFormatText = string.Format(@"select * from {0} with(nolock)", toPelicanFormatDbTable);
            List<PostFormatEntity> formatEntitiesList = new List<PostFormatEntity>();
            List<DeliveryRequestEntity> deliveryRequestEntities = new List<DeliveryRequestEntity>();

            using (SqlCommand cmd = new SqlCommand(cmdFormatText))
            {
                cmd.Connection = new SqlConnection(AWSConnectionString);
                try
                {
                    cmd.Connection.Open();
                    using (SqlDataReader sr = cmd.ExecuteReader())
                    {
                        while (sr.Read())
                        {
                            PostFormatEntity entity = new PostFormatEntity();
                            entity.Id = int.Parse(sr["Id"].ToString());
                            entity.IsNecessary = bool.Parse(sr["is_necessary"].ToString());
                            entity.ColumnName = sr["column_name"].ToString();
                            entity.IsNumberOnly = bool.Parse(sr["is_number_only"].ToString());
                            entity.StartLocation = int.Parse(sr["start_location"].ToString());
                            entity.EndLocation = int.Parse(sr["end_location"].ToString());
                            entity.Remark = sr["remark"].ToString();

                            formatEntitiesList.Add(entity);
                        }

                    }
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    ExceptionRecord(1, e);
                    Console.WriteLine(e);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }


            //這段只抓正物流、來回件正物流
            string cmdText = string.Format(@"select * from tcDeliveryRequests d with(nolock) left join ttDeliveryRequestsRecord r with(nolock) on d.request_id = r.request_id where d.area_arrive_code = '99' and d.print_date >= '{0}'
                                                and d.deliveryType='D' and d.print_date >= '{1}' and d.customer_code not like 'F2900210%' and d.check_number != '' and d.check_number not in (select jf_check_number from Pelican_request with(nolock))
                                                ", latestSuccessTime.AddDays(-1).ToString("yyyy-MM-dd"), scheduleStartDate.ToString("yyyy-MM-dd"));              //多抓一天防凌晨十二點，***記得拿掉測試字串
            using (SqlCommand cmd = new SqlCommand(cmdText))
            {
                cmd.Connection = new SqlConnection(AWSConnectionString);
                try
                {
                    cmd.Connection.Open();
                    using (SqlDataReader sr = cmd.ExecuteReader())
                    {
                        while (sr.Read())
                        {
                            DeliveryRequestEntity entity = new DeliveryRequestEntity()
                            {
                                JFRequestId = sr["request_id"].ToString(),
                                JFCheckNumber = sr["check_number"].ToString(),
                                OrderNumber = sr["order_number"].ToString(),
                                Pieces = sr["pieces"].ToString() == "" ? "1" : sr["pieces"].ToString(),
                                SendContact = LYName,
                                SendTel = LYTel,
                                SendCity = LYCity,
                                SendArea = LYArea,
                                SendAddress = LYAddress,
                                ReceiveContact = sr["receive_contact"].ToString(),
                                ReceiveTel = sr["receive_tel1"].ToString(),
                                ReceiveCity = sr["receive_city"].ToString(),
                                ReceiveArea = sr["receive_area"].ToString(),
                                ReceiveAddress = sr["receive_address"].ToString(),
                                ArriveAssignDate = sr["arrive_assign_date"].ToString() == "" ? sr["arrive_assign_date"].ToString() : DateTime.Parse(sr["arrive_assign_date"].ToString()).ToString("yyyy-MM-dd HH:dd:ss"),
                                AssignedTimePeriod = DefineMorningOrAfternoon(sr["time_period"].ToString()),
                                CollectionMoney = sr["collection_money"].ToString() == "" ? "0" : sr["collection_money"].ToString(),
                                DeliveryType = sr["deliveryType"].ToString(),
                                Remark = sr["invoice_desc"].ToString(),
                                RoundTrip = sr["round_trip"].ToString(),
                                DrRequestId = sr["request_id"].ToString(),
                                RoundTripCheckNumber = sr["roundtrip_checknumber"].ToString().Length > 0 ? sr["roundtrip_checknumber"].ToString() : ""
                            };
                            int temp_pieces;
                            if (entity.DeliveryType == "R" && int.TryParse(entity.Pieces, out temp_pieces))
                            {
                                int piece = int.Parse(entity.Pieces);
                                entity.Pieces = "1";
                                for (var i = 0; i < piece; i++)
                                {
                                    deliveryRequestEntities.Add(entity);
                                }
                            }
                            else
                            {
                                deliveryRequestEntities.Add(entity);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    ExceptionRecord(1, e);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            //以下這段是逆物流，不含來回件逆物流
            string cmdTextReturn = string.Format(@"select * from tcDeliveryRequests d with(nolock) left join ttDeliveryRequestsRecord r with(nolock) on d.request_id = r.request_id where d.send_station_scode = '99' and d.print_date >= '{0}'
                                                and d.deliveryType='R' and (d.check_number like '990%' or d.check_number like '202%') and d.print_date >= '{1}' and d.customer_code not like 'F2900210%' and d.check_number != '' and d.round_trip = 0 and d.check_number not in (select jf_check_number from Pelican_request with(nolock))
                                                ", latestSuccessTime.AddDays(-1).ToString("yyyy-MM-dd"), scheduleStartDate.ToString("yyyy-MM-dd"));     //多抓一天防凌晨十二點，***記得拿掉測試字串

            using (SqlCommand cmd = new SqlCommand(cmdTextReturn))
            {
                cmd.Connection = new SqlConnection(AWSConnectionString);
                try
                {
                    cmd.CommandTimeout = 2400;
                    cmd.Connection.Open();
                    using (SqlDataReader sr = cmd.ExecuteReader())
                    {
                        while (sr.Read())
                        {
                            DeliveryRequestEntity entity = new DeliveryRequestEntity()
                            {
                                JFRequestId = sr["request_id"].ToString(),
                                JFCheckNumber = sr["check_number"].ToString(),
                                OrderNumber = sr["order_number"].ToString(),
                                Pieces = sr["pieces"].ToString() == "" ? "1" : sr["pieces"].ToString(),
                                SendContact = sr["send_contact"].ToString(),
                                SendTel = sr["send_tel"].ToString(),
                                SendCity = sr["send_city"].ToString(),
                                SendArea = sr["send_area"].ToString(),
                                SendAddress = sr["send_address"].ToString(),
                                ReceiveContact = LYName,
                                ReceiveTel = LYTel,
                                ReceiveCity = LYCity,
                                ReceiveArea = LYArea,
                                ReceiveAddress = LYAddress,
                                ArriveAssignDate = sr["arrive_assign_date"].ToString() == "" ? sr["arrive_assign_date"].ToString() : DateTime.Parse(sr["arrive_assign_date"].ToString()).ToString("yyyy-MM-dd HH:dd:ss"),
                                AssignedTimePeriod = DefineMorningOrAfternoon(sr["time_period"].ToString()),
                                CollectionMoney = sr["collection_money"].ToString() == "" ? "0" : sr["collection_money"].ToString(),
                                DeliveryType = sr["deliveryType"].ToString(),
                                Remark = sr["invoice_desc"].ToString(),
                                RoundTrip = sr["round_trip"].ToString(),
                                DrRequestId = sr["request_id"].ToString(),
                                RoundTripCheckNumber = sr["roundtrip_checknumber"].ToString().Length > 0 ? sr["roundtrip_checknumber"].ToString() : ""
                            };
                            int temp_pieces;
                            if (entity.DeliveryType == "R" && int.TryParse(entity.Pieces, out temp_pieces))
                            {
                                int piece = int.Parse(entity.Pieces);
                                entity.Pieces = "1";
                                for (var i = 0; i < piece; i++)
                                {
                                    deliveryRequestEntities.Add(entity);
                                }
                            }
                            else
                            {
                                deliveryRequestEntities.Add(entity);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    ExceptionRecord(1, e);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            //以下這段是來回件逆物流
            string cmdTextRoundTripReturn = string.Format(@"select d.request_id, d.check_number,d.order_number,d.pieces,d.send_contact,d.send_tel,d.send_city,d.send_area,d.send_address,d.arrive_assign_date,d.time_period,d.collection_money,d.DeliveryType,d.invoice_desc,d.round_trip,d.roundtrip_checknumber,max(sl.scan_date) from tcDeliveryRequests d with(nolock) left join ttDeliveryRequestsRecord r with(nolock) on d.request_id = r.request_id inner join ttDeliveryScanLog sl with(nolock) on d.roundtrip_checknumber = sl.check_number where d.send_station_scode = '99' and d.print_date >= '{0}'
                                                and d.deliveryType='R' and d.check_number like '990%' and d.print_date >= '{1}' and d.customer_code not like 'F2900210%' and d.check_number != '' and sl.scan_item = '7' and round_trip = 1 and LEN(d.roundtrip_checknumber) > 1 and sl.driver_code = 'PP990' and d.check_number not in (select jf_check_number from Pelican_request with(nolock)) group by d.request_id, d.check_number,d.order_number,d.pieces,d.send_contact,d.send_tel,d.send_city,d.send_area,d.send_address,d.arrive_assign_date,d.time_period,d.collection_money,d.DeliveryType,d.invoice_desc,d.round_trip,d.roundtrip_checknumber
                                                ", latestSuccessTime.AddDays(-1).ToString("yyyy-MM-dd"), scheduleStartDate.ToString("yyyy-MM-dd"));     //多抓一天防凌晨十二點，***記得拿掉測試字串

            using (SqlCommand cmd = new SqlCommand(cmdTextRoundTripReturn))
            {
                cmd.Connection = new SqlConnection(AWSConnectionString);
                try
                {
                    cmd.CommandTimeout = 2400;
                    cmd.Connection.Open();
                    using (SqlDataReader sr = cmd.ExecuteReader())
                    {
                        while (sr.Read())
                        {
                            DeliveryRequestEntity entity = new DeliveryRequestEntity()
                            {
                                JFRequestId = sr["request_id"].ToString(),
                                JFCheckNumber = sr["check_number"].ToString(),
                                OrderNumber = sr["order_number"].ToString(),
                                Pieces = sr["pieces"].ToString() == "" ? "1" : sr["pieces"].ToString(),
                                SendContact = sr["send_contact"].ToString(),
                                SendTel = sr["send_tel"].ToString(),
                                SendCity = sr["send_city"].ToString(),
                                SendArea = sr["send_area"].ToString(),
                                SendAddress = sr["send_address"].ToString(),
                                ReceiveContact = LYName,
                                ReceiveTel = LYTel,
                                ReceiveCity = LYCity,
                                ReceiveArea = LYArea,
                                ReceiveAddress = LYAddress,
                                ArriveAssignDate = sr["arrive_assign_date"].ToString() == "" ? sr["arrive_assign_date"].ToString() : DateTime.Parse(sr["arrive_assign_date"].ToString()).ToString("yyyy-MM-dd HH:dd:ss"),
                                AssignedTimePeriod = DefineMorningOrAfternoon(sr["time_period"].ToString()),
                                CollectionMoney = sr["collection_money"].ToString() == "" ? "0" : sr["collection_money"].ToString(),
                                DeliveryType = sr["deliveryType"].ToString(),
                                Remark = sr["invoice_desc"].ToString(),
                                RoundTrip = sr["round_trip"].ToString(),
                                DrRequestId = sr["request_id"].ToString(),
                                RoundTripCheckNumber = sr["roundtrip_checknumber"].ToString().Length > 0 ? sr["roundtrip_checknumber"].ToString() : ""
                            };
                            int temp_pieces;
                            if (entity.DeliveryType == "R" && int.TryParse(entity.Pieces, out temp_pieces))
                            {
                                int piece = int.Parse(entity.Pieces);
                                entity.Pieces = "1";
                                for (var i = 0; i < piece; i++)
                                {
                                    deliveryRequestEntities.Add(entity);
                                }
                            }
                            else
                            {
                                deliveryRequestEntities.Add(entity);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    ExceptionRecord(1, e);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }


            List<PostZipEntity> zipList = new List<PostZipEntity>();
            string cmdZipText = @"select * from ttArriveSites with(nolock)";
            using (SqlCommand cmd = new SqlCommand(cmdZipText))
            {
                cmd.Connection = new SqlConnection(AWSConnectionString);
                try
                {
                    cmd.Connection.Open();
                    using (SqlDataReader sr = cmd.ExecuteReader())
                    {
                        while (sr.Read())
                        {
                            PostZipEntity pzEntity = new PostZipEntity()
                            {
                                PostArea = sr["post_area"].ToString(),
                                PostCity = sr["post_city"].ToString(),
                                Zip = sr["zip"].ToString()
                            };
                            zipList.Add(pzEntity);
                        }

                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("取得3碼郵遞區號失敗");
                    isSuccess = false;
                    ExceptionRecord(1, e);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            int total_fail = 0;
            int fail = 0;
            for (var i = 0; i < deliveryRequestEntities.Count; i++)
            {
                try
                {
                    PelicanCreateReuturnEntity apiReturnEntity = ApiRequestToPelican(deliveryRequestEntities[i].ReceiveCity + deliveryRequestEntities[i].ReceiveArea + deliveryRequestEntities[i].ReceiveAddress, deliveryRequestEntities[i].ReceiveZip3);
                    deliveryRequestEntities[i].IsNeedToInsertTable = apiReturnEntity.IsSuccess;

                    if (deliveryRequestEntities[i].IsNeedToInsertTable)     //若沒有取得宅配通郵遞區號則不寫入資料表
                    {
                        JfCustomerCodeAndPelicanCheckNumber temp = deliveryRequestEntities[i].DeliveryType == "D" ? GetPelicanCheckNumber(false, false, deliveryRequestEntities[i].JFCheckNumber) : GetPelicanCheckNumber(true, deliveryRequestEntities[i].RoundTrip.ToLower() == "true", deliveryRequestEntities[i].JFCheckNumber);
                        deliveryRequestEntities[i].PelicanCheckNumber = temp.PelicanCheckNumber.PadLeft(12, ' ');
                        deliveryRequestEntities[i].JfCustomerCode = temp.JfCustomerCode;
                        deliveryRequestEntities[i].SendZip3 = FromAddressToZip(deliveryRequestEntities[i].SendCity, deliveryRequestEntities[i].SendArea, zipList);
                        deliveryRequestEntities[i].ReceiveZip3 = FromAddressToZip(deliveryRequestEntities[i].ReceiveCity, deliveryRequestEntities[i].ReceiveArea, zipList);

                        deliveryRequestEntities[i].PelicanReceiveZip5 = apiReturnEntity.PZip5;
                        deliveryRequestEntities[i].PelicanReceiveArea = apiReturnEntity.Area;
                        deliveryRequestEntities[i].Remark = (deliveryRequestEntities[i].RoundTrip.ToLower() == "true" ? "回件:" + deliveryRequestEntities[i].PelicanCheckNumber.Substring(0, 1) + "84" + deliveryRequestEntities[i].PelicanCheckNumber.Substring(3, 9) : "") + deliveryRequestEntities[i].Remark;
                    }
                    fail = 0;
                }
                catch (Exception e)
                {
                    total_fail += 1;
                    fail += 1;
                    Console.WriteLine(deliveryRequestEntities[i].JFRequestId);
                    if (fail < failTimes)
                    {
                        i--;
                    }
                    ExceptionRecord(1, e);
                }
                Console.WriteLine("處理API段中..." + i + @"/" + deliveryRequestEntities.Count + "，失敗" + total_fail + "次");
            }

            deliveryRequestEntities = deliveryRequestEntities.Where(d => !(d.PelicanCheckNumber is null) && d.IsNeedToInsertTable).ToList();      //避免超過貨號的資料也傳進去

            try
            {
                Alertor(deliveryRequestEntities.Where(x => x.DeliveryType == "D").Count(), deliveryRequestEntities.Where(x => x.DeliveryType == "R").Count());      //警示器
            }
            catch (Exception ex)
            {

            }
            TraslateAndExportToTxt(deliveryRequestEntities, formatEntitiesList);

            for (var i = 0; i < deliveryRequestEntities.Count; i++)
            {
                //deliveryRequestEntities[i] = CheckDeliveryRequestContainsSQLIllegle(deliveryRequestEntities[i]);      //這段已不需要，防止客戶輸入特殊字元，但有BUG仙註解掉
                if (!InsertRequestNewRequests(deliveryRequestEntities[i], insertId.ToString()))
                {
                    isSuccess = false;
                };
                Console.WriteLine("已嵌入Pelican_request" + (i + 1) + @"/" + deliveryRequestEntities.Count);
            }


            WriteDbLog(1, isSuccess, true, insertId);


        }

        private static string DefineMorningOrAfternoon(string input)
        {
            if (input.Contains("早"))
            {
                return "01";
            }
            else if (input.Contains("午") || input.Contains("晚"))
            {
                return "02";
            }
            else                    //不指定
            {
                return "00";
            }
        }

        private static void TraslateAndExportToTxt(List<DeliveryRequestEntity> inputList, List<PostFormatEntity> formatEntities)
        {
            //先檢查是否符合格式
            int id = WriteDbLog(2, false, false);
            bool isSuccess = true;
            for (var i = 0; i < inputList.Count; i++)
            {
                inputList[i].Pieces = LengthTrimer(inputList[i].Pieces, 3);
                inputList[i].OrderNumber = LengthTrimer(inputList[i].OrderNumber, 32);
                inputList[i].SendContact = LengthTrimer(((inputList[i].RoundTrip.ToLower() == "true" && inputList[i].DeliveryType == "R") ? "[來回]" : "") + inputList[i].SendContact, 32);
                inputList[i].SendTel = LengthTrimer(inputList[i].SendTel, 32);
                inputList[i].ReceiveContact = LengthTrimer(((inputList[i].RoundTrip.ToLower() == "true" && inputList[i].DeliveryType == "D") ? "[來回]" : "") + inputList[i].ReceiveContact, 32);
                inputList[i].ReceiveTel = LengthTrimer(inputList[i].ReceiveTel, 32);
                inputList[i].CollectionMoney = LengthTrimer(inputList[i].CollectionMoney, 8);
                inputList[i].Remark = LengthTrimer(inputList[i].Remark, 60);
                if (inputList[i].SendCity.Length + inputList[i].SendArea.Length + inputList[i].SendAddress.Length > 64)
                {
                    int addressResidueLength = 64 - inputList[i].SendCity.Length - inputList[i].SendArea.Length;
                    inputList[i].SendAddress = LengthTrimer(inputList[i].SendAddress, addressResidueLength);
                }
                if (inputList[i].ReceiveCity.Length + inputList[i].ReceiveArea.Length + inputList[i].ReceiveAddress.Length > 64)
                {
                    int addressResidueLength = 64 - inputList[i].ReceiveCity.Length - inputList[i].ReceiveArea.Length;
                    inputList[i].ReceiveAddress = LengthTrimer(inputList[i].ReceiveAddress, addressResidueLength);
                }

            }

            string resultD = "";
            string resultR = "";
            string resultP_D = "";
            string resultP_R = "";
            int no_D = 0; //序號
            int no_R = 0;   //逆物流序號
            int no_P_D = 0; //來回件
            int no_P_R = 0;
            //List<string> resultList = new List<string>();
            //List<string> resultListReturn = new List<string>();
            for (var i = 0; i < inputList.Count; i++)
            {
                string result = "";
                if (inputList[i].RoundTrip.ToLower() != "true")      //是否為來回件
                {
                    if (inputList[i].DeliveryType == "R")           //正逆物流分開計
                    {
                        no_R += 1;
                        result += TransUTF8ToBig5ByteAndTrimLength(no_R.ToString(), 4, true);
                    }
                    else
                    {
                        no_D += 1;
                        result += TransUTF8ToBig5ByteAndTrimLength(no_D.ToString(), 4, true);
                    }
                }
                else //if (inputList[i].RoundTripCheckNumber != "")     //因為DeliveryRequests的逆物流roundtrip_checknumber沒有處理好，所以先暫時註解掉
                {
                    if (inputList[i].DeliveryType == "R")
                    {
                        no_P_R += 1;
                        result += TransUTF8ToBig5ByteAndTrimLength(no_P_R.ToString(), 4, true);
                    }
                    else
                    {
                        no_P_D += 1;
                        result += TransUTF8ToBig5ByteAndTrimLength(no_P_D.ToString(), 4, true);
                    }
                }

                result += TransUTF8ToBig5ByteAndTrimLength(DateTime.Now.ToString("yyyyMMdd"), 8, false);
                for (var j = 0; j < 8; j++)
                {
                    result += " ";
                }
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].PelicanCheckNumber, 12, false);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].Pieces.ToString(), 3, true);
                result += "01";             //設定S60
                result += inputList[i].JfCustomerCode;               //客戶代號
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].JFCheckNumber.ToString(), 32, false, true);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].SendContact, 32, false, true);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].SendTel, 32, false, true);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].SendZip3, 3, false);
                result += TransUTF8ToBig5ByteAndTrimLength((inputList[i].SendCity + inputList[i].SendArea + inputList[i].SendAddress), 64, false, true);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].ReceiveContact, 32, false, true);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].ReceiveTel, 32, false, true);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].ReceiveZip3, 3, false);
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].ReceiveCity + inputList[i].ReceiveArea + inputList[i].ReceiveAddress, 64, false, true);
                for (var j = 0; j < 30; j++)
                {
                    result += " ";
                }
                for (var j = 0; j < 10; j++)
                {
                    result += " ";
                }
                for (var j = 0; j < 8; j++)
                {
                    result += " ";
                }
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].AssignedTimePeriod, 2, false);
                for (var j = 0; j < 1; j++)
                {
                    result += "1";
                }
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].CollectionMoney.ToString(), 8, true);
                result += "01";      //商品別，01一般
                result += inputList[i].PelicanReceiveZip5;              //收件地址分貨碼
                for (var j = 0; j < 1; j++)
                {
                    result += " ";
                }
                result += inputList[i].DeliveryType.ToString() == "R" ? "2" : "1";
                result += TransUTF8ToBig5ByteAndTrimLength(inputList[i].Remark, 60, false, true);
                result += inputList[i].CollectionMoney == "0" ? "0" : "1";
                for (var j = 0; j < 42; j++)
                {
                    result += " ";
                }
                result = TransUTF8ToBig5ByteAndTrimLength(result, 512, false);
                result += "\r\n";

                if (inputList[i].RoundTrip.ToLower() != "true")
                {
                    if (inputList[i].DeliveryType == "R")
                    {
                        //resultListReturn.Add(result);
                        resultR += result;
                    }
                    else
                    {
                        //resultList.Add(result);
                        resultD += result;
                    }
                }
                else
                {
                    if (inputList[i].DeliveryType == "R")
                    {
                        //resultListReturn.Add(result);
                        resultP_R += result;
                    }
                    else
                    {
                        //resultList.Add(result);
                        resultP_D += result;
                    }
                }

                int textLength = result.Length;                     //判定長度是否正確
                Console.WriteLine("本筆字串長度:" + textLength);
            }

            isSuccess = CreateFtpFile(resultD, id, 0) && CreateFtpFile(resultR, id, 1) && CreateFtpFile(resultP_D, id, 2) && CreateFtpFile(resultP_R, id, 3);


            WriteDbLog(2, isSuccess, true, id);
        }

        private static string LengthTrimer(string input, int length)
        {
            input = input.Replace("\r\n", "").Replace("\n", "").Replace("\t", "").Replace("\r", "");
            string result = input;
            if (input.Length > length)
            {
                result = input.Substring(0, length);
            }
            return result;
        }

        private static string SqlStringRegular(string str)           //除錯
        {
            if (str.Length > 0)
            {
                str = str.Replace("'", "''");
            }

            return str;
        }

        private static string FromAddressToZip(string City, string Area, List<PostZipEntity> zipList)
        {
            string zip = zipList.Where(x => x.PostCity.Equals(City) && x.PostArea.Equals(Area)).ToArray().Length != 0 ? zipList.Where(x => x.PostCity.Equals(City) && x.PostArea.Equals(Area)).FirstOrDefault().Zip : "";
            return zip;
        }

        public static PelicanCreateReuturnEntity ApiRequestToPelican(string address, string zip3)           //代入三碼郵遞區號，用在API無法辨識時使用
        {
            PelicanCreateReuturnEntity result = new PelicanCreateReuturnEntity();
            string patternString = "\"PZip5\":\"";
            WebRequest wr = WebRequest.Create(@"http://query1.e-can.com.tw:8080/datasnap/rest/tservermt/lookupzip/" + address);
            HttpWebResponse response = (HttpWebResponse)wr.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd().Replace("{\"result\":[", "").Replace("]}", "");
            /*int indexForResult = responseFromServer.IndexOf(patternString);           //用Jsonconvert代替
            result = responseFromServer.Substring(indexForResult + 9, 5);*/
            result.PZip5 = JsonConvert.DeserializeObject<PelicanCreateReuturnEntity>(responseFromServer).PZip5;
            result.Area = JsonConvert.DeserializeObject<PelicanCreateReuturnEntity>(responseFromServer).Area;
            reader.Close();
            dataStream.Close();
            response.Close();
            int temp;
            if (result.PZip5.Length == 5 && int.TryParse(result.PZip5, out temp))
            {
                result.IsSuccess = true;
                return result;
            }
            else
            {
                //result.PZip5 = zip3 + "00";
                result.IsSuccess = false;
                return result;
            }
        }

        private static int WriteDbLog(int step, bool isSuccess, bool isEnd, int request_id = -1)           //false:start true:end
        {
            string cmdText = "";
            if (!isEnd)
            {
                string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                cmdText = string.Format(@"insert into {0} (cdate, start_time, step) values ('{1}','{1}','{2}') select @@IDENTITY as id", logPelicanSchedule, now, step);
            }
            else
            {
                string boolToBit = isSuccess == true ? "1" : "0";
                string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                cmdText = string.Format(@"update {0} set is_success = {1}, end_time = '{2}' where id = {3} select @@IDENTITY as id", logPelicanSchedule, boolToBit, now, request_id);
            }

            int return_id = -1;
            using (SqlCommand cmd = new SqlCommand(cmdText))
            {
                try
                {
                    cmd.Connection = new SqlConnection(AWSConnectionString);
                    cmd.Connection.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (!isEnd)
                        {
                            while (dr.Read())
                            {
                                return_id = int.Parse(dr["id"].ToString());
                            }
                        }
                    }
                }
                catch (Exception e)
                {

                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return return_id;
        }

        private static bool InsertRequestNewRequests(DeliveryRequestEntity entity, string logId)
        {
            bool isSuccess = true;

            string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string failTime = DateTime.Now.ToString("yyyyMMddHHmmss");

            string cmdText = "";
            if (entity.ArriveAssignDate == "")
            {
                cmdText = string.Format(@"insert into {0} (log_id, pelican_check_number,jf_request_id,jf_check_number,delivery_type,cdate,udate,cuser,uuser,fail_times,fail_log_id,fail_at,order_number,pieces,send_contact,send_tel,send_city,send_area,send_address,receive_contact,receive_tel,receive_city,receive_area,receive_address,assigned_time_period,collection_money,remark,jf_customer_code,jf_send_zip3,jf_receive_zip3,pelican_receive_zip5,pelican_receive_area,round_trip,dr_request_id,roundtrip_checknumber) values ({1},{2},'{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10},{11},{12},'{13}',{14},'{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}',{26},'{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}')", requestPelican, logId, entity.PelicanCheckNumber, entity.JFRequestId, entity.JFCheckNumber, entity.DeliveryType, now, now, "schedule", "schedule", 0, -1, -1, SqlStringRegular(entity.OrderNumber), entity.Pieces, SqlStringRegular(entity.SendContact), SqlStringRegular(entity.SendTel), entity.SendCity, entity.SendArea, SqlStringRegular(entity.SendAddress), SqlStringRegular(entity.ReceiveContact), SqlStringRegular(entity.ReceiveTel), entity.ReceiveCity, entity.ReceiveArea, SqlStringRegular(entity.ReceiveAddress), entity.AssignedTimePeriod, entity.CollectionMoney, SqlStringRegular(entity.Remark), entity.JfCustomerCode, entity.SendZip3, entity.ReceiveZip3, entity.PelicanReceiveZip5, entity.PelicanReceiveArea, entity.RoundTrip, entity.DrRequestId, entity.RoundTripCheckNumber);
            }
            else
            {
                cmdText = string.Format(@"insert into {0} (log_id, pelican_check_number,jf_request_id,jf_check_number,delivery_type,cdate,udate,cuser,uuser,fail_times,fail_log_id,fail_at,order_number,pieces,send_contact,send_tel,send_city,send_area,send_address,receive_contact,receive_tel,receive_city,receive_area,receive_address,assigned_time_period,collection_money,remark,jf_customer_code,jf_send_zip3,jf_receive_zip3,pelican_receive_zip5,pelican_receive_area,round_trip,dr_request_id,roundtrip_checknumber,arrive_assign_date) values ({1},{2},'{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10},{11},{12},'{13}',{14},'{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}',{26},'{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}')", requestPelican, logId, entity.PelicanCheckNumber, entity.JFRequestId, entity.JFCheckNumber, entity.DeliveryType, now, now, "schedule", "schedule", 0, -1, -1, SqlStringRegular(entity.OrderNumber), entity.Pieces, SqlStringRegular(entity.SendContact), SqlStringRegular(entity.SendTel), entity.SendCity, entity.SendArea, SqlStringRegular(entity.SendAddress), SqlStringRegular(entity.ReceiveContact), SqlStringRegular(entity.ReceiveTel), entity.ReceiveCity, entity.ReceiveArea, SqlStringRegular(entity.ReceiveAddress), entity.AssignedTimePeriod, entity.CollectionMoney, SqlStringRegular(entity.Remark), entity.JfCustomerCode, entity.SendZip3, entity.ReceiveZip3, entity.PelicanReceiveZip5, entity.PelicanReceiveArea, entity.RoundTrip, entity.DrRequestId, entity.RoundTripCheckNumber, entity.ArriveAssignDate);
            }
            using (SqlCommand cmd = new SqlCommand(cmdText))
            {
                try
                {
                    cmd.Connection = new SqlConnection(AWSConnectionString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(entity.JFRequestId + e.ToString());

                    try
                    {
                        if (!File.Exists(logPelicanFailInsertIntoPelicanRequest.Replace(".txt", "") + "_" + failTime + ".txt"))
                        {
                            var newFile = File.Create(logPelicanFailInsertIntoPelicanRequest.Replace(".txt", "") + "_" + failTime + ".txt");
                            newFile.Close();
                        }
                        string[] failDetail = new string[]
                        {
                        cmdText,
                        "----------------------------------------------------------------"
                        };

                        File.AppendAllLines(logPelicanFailInsertIntoPelicanRequest.Replace(".txt", "") + "_" + failTime + ".txt", failDetail);
                    }
                    catch
                    {

                    }
                    isSuccess = false;
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
            return isSuccess;
        }

        private static JfCustomerCodeAndPelicanCheckNumber GetPelicanCheckNumber(bool isReturnDelivery, bool isRoundTripReturn, string fseDeliveryCheckNumber)      //false:正物流 true:逆物流/true:來回件的"回件"
        {
            JfCustomerCodeAndPelicanCheckNumber result = new JfCustomerCodeAndPelicanCheckNumber();
            bool isExceed = false;      //是否超過結束單號
            string cmdText = @"";
            if (isRoundTripReturn)      //來回件的回件比較特別，不取單號，從其宅配通正物流單號2、3碼改成"84"
            {
                cmdText = string.Format(@"select SUBSTRING(pelican_check_number, 1, 1) + '84' + SUBSTRING(pelican_check_number, 4, 9)'number_next', i.customer_code, i.number_end from {0} d with(nolock) left join {1} p with(nolock) on d.check_number = p.roundtrip_checknumber left join {2} i with(nolock) on p.jf_customer_code = i.customer_code where i.delivery_type = 'R' and active_flag = 1 and company_id = {3}
                and check_number = '{4}'", TcDeliveryRequest, requestPelican, intermodalTransportationCheckNumber, companyIdPelican, fseDeliveryCheckNumber);
            }
            else if (!isReturnDelivery)
            {
                cmdText = string.Format(@"select * from {0} with(nolock) where active_flag = 1 and delivery_type = 'D' and company_id = {1} and priority = (select min(priority) from {0} where active_flag = 1 and delivery_type = 'D' and company_id = {1} and number_end>=number_next)
                            update {0} set number_next = convert(varchar, convert(bigint,SUBSTRING(number_next,0,12)) + 1) + convert(varchar,(convert(bigint,SUBSTRING(number_next,0,12))+1)%7) where active_flag = 1 and delivery_type = 'D' and company_id = {1} and priority = (select min(priority) from {0} where active_flag = 1 and delivery_type = 'D' and company_id = {1} and number_end>=number_next)", intermodalTransportationCheckNumber, companyIdPelican);
            }
            else
            {
                cmdText = string.Format(@"select * from {0} with(nolock) where active_flag = 1 and delivery_type = 'R' and company_id = {1} and priority = (select min(priority) from {0} where active_flag = 1 and delivery_type = 'R' and company_id = {1} and number_end>=number_next)
                            update {0} set number_next = convert(varchar, convert(bigint,SUBSTRING(number_next,0,12)) + 1) + convert(varchar,(convert(bigint,SUBSTRING(number_next,0,12))+1)%7) where active_flag = 1 and delivery_type = 'R' and company_id = {1} and priority = (select min(priority) from {0} where active_flag = 1 and delivery_type = 'R' and company_id = {1} and number_end>=number_next)", intermodalTransportationCheckNumber, companyIdPelican);
            }
            using (SqlCommand cmd = new SqlCommand(cmdText))
            {
                try
                {
                    cmd.Connection = new SqlConnection(AWSConnectionString);

                    cmd.Connection.Open();
                    int records = 0;
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            result.PelicanCheckNumber = dr["number_next"].ToString();
                            result.JfCustomerCode = dr["customer_code"].ToString();
                            result.NumberEnd = dr["number_end"].ToString();
                            records++;
                        }
                        if (records == 0)
                        {
                            isExceed = true;
                        }
                    }
                }
                catch (Exception e)
                {
                    ExceptionRecord(999, e);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
            if (isExceed)
            {
                try
                {
                    WriteDbLog(999, false, false);       //超過貨號區間了
                }
                catch
                {

                }
            }
            return result;
        }

        private static string GetBarcodeImage(string input)     //沒在用
        {
            string SavePath = @"E:\barcode.png";
            //Barcode条码需在前后加上*字号代表开始与结束
            Bitmap barcode = CreateBarCode("*" + input + "*");
            barcode.Save(SavePath, ImageFormat.Png);
            barcode.Dispose();

            //将图片文件转成Base64字符串
            using (var fs = new FileStream(SavePath, FileMode.Open, FileAccess.Read))
            {
                var buffer = new byte[fs.Length];
                fs.Read(buffer, 0, (int)fs.Length);
                string base64String = Convert.ToBase64String(buffer);
                string ImgBase64 = string.Format("data:image/png;base64,{0}'", base64String);
                return ImgBase64;
            }



        }

        public static Bitmap CreateBarCode(string data)
        {
            Bitmap barcode = new Bitmap(1, 1);
            Font threeOfNine = new Font("IDAutomationHC39M", 60,
                                    System.Drawing.FontStyle.Regular,
                                    System.Drawing.GraphicsUnit.Point);

            Graphics graphics = Graphics.FromImage(barcode);

            SizeF dataSize = graphics.MeasureString(data, threeOfNine);

            barcode = new Bitmap(barcode, dataSize.ToSize());
            graphics = Graphics.FromImage(barcode);
            graphics.Clear(Color.White);

            graphics.TextRenderingHint = TextRenderingHint.SingleBitPerPixel;

            graphics.DrawString(data, threeOfNine, new SolidBrush(Color.Black), 0, 0);

            graphics.Flush();
            threeOfNine.Dispose();
            graphics.Dispose();

            return barcode;

        }

        public static string TransUTF8ToBig5ByteAndTrimLength(string content, int lengthLimit, bool isFillWithZero, bool isFillFromRight = false)        //emptyOrZero 不足長度補零或是空白 empty:空白 true:補零，isFillFromRight false:從左邊開始補 true:從右邊開始補
        {
            int lengthCalculatorByte = 0;
            byte[] byteResult = new byte[0];
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            for (var i = 0; i < content.Length; i++)
            {
                string temp = content.Substring(i, 1);
                byte[] tempByte = Encoding.GetEncoding("big5").GetBytes(temp);
                lengthCalculatorByte += tempByte.Length;
                if (lengthCalculatorByte > lengthLimit)
                {
                    break;
                }
                byteResult = byteResult.Concat(tempByte).ToArray();
            }
            int nowByteLength = byteResult.Length;
            for (var j = 0; j < lengthLimit - nowByteLength; j++)
            {
                if (isFillWithZero)
                {
                    if (isFillFromRight)
                    {
                        byteResult = byteResult.Concat(Encoding.GetEncoding("big5").GetBytes("0")).ToArray();
                    }
                    else
                    {
                        byteResult = Encoding.GetEncoding("big5").GetBytes("0").Concat(byteResult).ToArray();
                    }
                }
                else
                {
                    if (isFillFromRight)
                    {
                        byteResult = byteResult.Concat(Encoding.GetEncoding("big5").GetBytes(" ")).ToArray();
                    }
                    else
                    {
                        byteResult = Encoding.GetEncoding("big5").GetBytes(" ").Concat(byteResult).ToArray();
                    }
                }
            }
            string result = Encoding.GetEncoding("big5").GetString(byteResult);

            return result;
        }

        public static void ReadReturnFile()
        {
            int insertId = WriteDbLog(3, false, false);
            bool isSuccess = true;
            bool isLogSuccess = true;

            //這裡寫取得ftp

            DirectoryInfo dirs = new DirectoryInfo(ftpReturnPath);
            IEnumerable<FileInfo> files = dirs.GetFiles("*.txt");
            List<PelicanStatusEntity> statusEntities = new List<PelicanStatusEntity>();
            List<TtDeliveryScanLogEntity> thisTimeInsertIntoScanLog = new List<TtDeliveryScanLogEntity>();

            string cmdStatusText = string.Format(@"select * from {0} with(nolock)", pelicanStatusTable);
            using (SqlCommand cmd = new SqlCommand(cmdStatusText))
            {
                cmd.Connection = new SqlConnection(AWSConnectionString);
                cmd.Connection.Open();
                try
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PelicanStatusEntity status = new PelicanStatusEntity()
                            {
                                ArriveOption = reader["arrive_option"].ToString(),
                                ScanItem = reader["scan_item"].ToString(),
                                PelicanStatusCode = reader["pelican_status_code"].ToString(),
                                IsDeliveryD = reader["is_delivery_D"].ToString().ToLower() == "true" ? true : false,
                                IsDeliveryR = reader["is_delivery_R"].ToString().ToLower() == "true" ? true : false,
                                EndCode = reader["pelican_end_code"].ToString()
                            };
                            statusEntities.Add(status);
                        }
                    }
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    ExceptionRecord(3, e);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            foreach (var f in files)
            {
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);                      //這裡讀取回覆檔內容
                bool isDFile = !f.Name.Contains("R" + JFCustomerCode);//是否為正物流
                using (StreamReader sourceStream = new StreamReader(f.FullName, Encoding.GetEncoding("big5")))
                {
                    byte[] fileContent = Encoding.GetEncoding("big5").GetBytes(sourceStream.ReadToEnd());
                    int records = fileContent.Length / 578;                 //每筆長度578
                    for (var i = 0; i < records; i++)
                    {
                        byte[] thisRecord = new byte[576];
                        Array.Copy(fileContent, i * 578, thisRecord, 0, 576);       //加上換行符號長度

                        string endCode = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 506, 1)).Trim(' ');
                        string statusCode = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 507, 4)).Trim(' ');
                        string statusCodeDetail = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 542, 5)).Trim(' ');
                        string scanDate = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 518, 14)).Trim(' ');
                        try
                        {
                            ScanItemAndArriveOptionEntity sa = ReturnSA(endCode, statusCode, statusCodeDetail, statusEntities, isDFile);

                            TtDeliveryScanLogEntity scanLog = new TtDeliveryScanLogEntity()
                            {
                                AreaArriveCode = "99",
                                ArriveOption = sa.ArriveOption,
                                CDate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"),
                                CheckNumber = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 25, 32)).Trim(' '),
                                DriverCode = scanLogDriverCode,
                                ScanDate = scanDate.Insert(12, ":").Insert(10, ":").Insert(8, " ").Insert(6, "-").Insert(4, "-"),
                                ScanItem = sa.ScanItem,
                                ScanLogId = "",
                                ReturnFilename = f.Name,
                                AccountCode = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 0, 10)),
                                PelicanCheckNumber = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 10, 12)),
                                ReceiveZip3 = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 254, 3)),
                                InformationCreateDate = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 341, 8)).Insert(6, "-").Insert(4, "-"),
                                ReceiveDate = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 349, 8)).Trim(' ').Length > 0 ? Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 349, 8)).Insert(6, "-").Insert(4, "-") : "",
                                ReturnFileCreateTime = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 492, 14)).Insert(12, ":").Insert(10, ":").Insert(8, " ").Insert(6, "-").Insert(4, "-"),
                                EndCode = endCode,
                                StatusCode = statusCode,
                                ScanStationCode = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 511, 3)),
                                ScanStationShortname = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 514, 4)),
                                StatusChinese = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 532, 8)),
                                ReceiveCode = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 540, 2)),
                                AbnormalStatusCode = statusCodeDetail,
                                AbnormalStatusChinese = Encoding.GetEncoding("big5").GetString(CatchByteArrayRangeByIndexRange(thisRecord, 547, 12)),
                                IsD = isDFile
                            };
                            thisTimeInsertIntoScanLog.Add(scanLog);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }

            int logInsertId = WriteDbLog(5, false, false);
            for (var i = 0; i < thisTimeInsertIntoScanLog.Count(); i++)
            {
                bool isInserted = false;     //歷程是否與上一筆相同
                string cmdCheckText = string.Format(@"select status_code, handy_time from {0} with(nolock) where id = (select max(id)'id' from {0} with(nolock) where fse_check_number = '{1}')", PelicanReturnContent, thisTimeInsertIntoScanLog[i].CheckNumber);
                using (SqlCommand cmd = new SqlCommand(cmdCheckText))
                {
                    try
                    {
                        cmd.Connection = new SqlConnection(AWSConnectionString);
                        cmd.Connection.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string latestHandyTime = reader["handy_time"] is DBNull ? "" : DateTime.Parse(reader["handy_time"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                                if (reader["status_code"].ToString() == thisTimeInsertIntoScanLog[i].StatusCode && latestHandyTime == thisTimeInsertIntoScanLog[i].ScanDate)
                                {
                                    isInserted = true;
                                };
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        isSuccess = false;
                        ExceptionRecord(3, e);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }

                if (!isInserted)
                {
                    string cmdText = string.Format(@"insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, arrive_option, cdate) values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}') select @@IDENTITY as id", thisTimeInsertIntoScanLog[i].DriverCode, thisTimeInsertIntoScanLog[i].CheckNumber, thisTimeInsertIntoScanLog[i].ScanItem, thisTimeInsertIntoScanLog[i].ScanDate, thisTimeInsertIntoScanLog[i].AreaArriveCode, thisTimeInsertIntoScanLog[i].ArriveOption, thisTimeInsertIntoScanLog[i].CDate);
                    if (!thisTimeInsertIntoScanLog[i].IsD)  //逆物流
                    {
                        cmdText = string.Format(@"insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, receive_option, cdate) values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}') select @@IDENTITY as id", thisTimeInsertIntoScanLog[i].DriverCode, thisTimeInsertIntoScanLog[i].CheckNumber, thisTimeInsertIntoScanLog[i].ScanItem, thisTimeInsertIntoScanLog[i].ScanDate, thisTimeInsertIntoScanLog[i].AreaArriveCode, thisTimeInsertIntoScanLog[i].ArriveOption, thisTimeInsertIntoScanLog[i].CDate);
                    }
                    else if (thisTimeInsertIntoScanLog[i].ScanItem == "7")      //正物流且為發送
                    {
                        cmdText = string.Format(@"insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, send_option, cdate) values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}') select @@IDENTITY as id", thisTimeInsertIntoScanLog[i].DriverCode, thisTimeInsertIntoScanLog[i].CheckNumber, thisTimeInsertIntoScanLog[i].ScanItem, thisTimeInsertIntoScanLog[i].ScanDate, thisTimeInsertIntoScanLog[i].AreaArriveCode, thisTimeInsertIntoScanLog[i].ArriveOption, thisTimeInsertIntoScanLog[i].CDate);
                    }

                    using (SqlCommand cmd = new SqlCommand(cmdText))
                    {
                        try
                        {
                            cmd.Connection = new SqlConnection(AWSConnectionString);
                            cmd.Connection.Open();
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    thisTimeInsertIntoScanLog[i].ScanLogId = reader["id"].ToString();
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            isSuccess = false;
                            ExceptionRecord(3, e);
                        }
                        finally
                        {
                            cmd.Connection.Close();
                        }
                    }
                    string cmdTextForLog = string.Format(@"insert into {0} (scan_log_id, write_in_time,	return_filename, account_code, pelican_check_number, fse_check_number, receive_zip3, information_create_date, receive_date,	return_file_create_time, end_code, status_code, scan_station_code, scan_station_shortname, handy_time, status_chinese, receive_code, abnormal_status_code, abnormal_status_chinese) values ('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}')", PelicanReturnContent, thisTimeInsertIntoScanLog[i].ScanLogId, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), SqlStringRegular(thisTimeInsertIntoScanLog[i].ReturnFilename), thisTimeInsertIntoScanLog[i].AccountCode, thisTimeInsertIntoScanLog[i].PelicanCheckNumber, thisTimeInsertIntoScanLog[i].CheckNumber, thisTimeInsertIntoScanLog[i].ReceiveZip3, thisTimeInsertIntoScanLog[i].InformationCreateDate, thisTimeInsertIntoScanLog[i].ReceiveDate, thisTimeInsertIntoScanLog[i].ReturnFileCreateTime, thisTimeInsertIntoScanLog[i].EndCode, thisTimeInsertIntoScanLog[i].StatusCode, thisTimeInsertIntoScanLog[i].ScanStationCode, thisTimeInsertIntoScanLog[i].ScanStationShortname, thisTimeInsertIntoScanLog[i].ScanDate, thisTimeInsertIntoScanLog[i].StatusChinese, thisTimeInsertIntoScanLog[i].ReceiveCode, thisTimeInsertIntoScanLog[i].AbnormalStatusCode, thisTimeInsertIntoScanLog[i].AbnormalStatusChinese);
                    using (SqlCommand cmdForLog = new SqlCommand(cmdTextForLog))
                    {
                        try
                        {
                            cmdForLog.Connection = new SqlConnection(AWSConnectionString);
                            cmdForLog.Connection.Open();
                            cmdForLog.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            isLogSuccess = false;
                            ExceptionRecord(5, e);
                        }
                        finally
                        {
                            cmdForLog.Connection.Close();
                        }
                    }
                }
            }

            if (isSuccess)
            {
                if (!Directory.Exists(ftpFileReturnRecord))
                {
                    Directory.CreateDirectory(ftpFileReturnRecord);
                }
                foreach (var f in files)
                {
                    if (!Directory.Exists(ftpFileReturnRecord))
                    {
                        Directory.CreateDirectory(ftpFileReturnRecord);
                    }
                    File.Move(ftpReturnPath + f.Name, ftpFileReturnRecord + f.Name.Replace(".txt", "_" + DateTime.Now.ToString("HHmm") + ".txt"));
                }
            }

            WriteDbLog(5, isSuccess, true, logInsertId);
            WriteDbLog(3, isSuccess, true, insertId);
        }

        private static void FtpUploaderToPelican()          //
        {
            bool isSuccess = true;
            int insertId = WriteDbLog(4, false, false);
            byte[] data;
            DirectoryInfo dirs = new DirectoryInfo(pathSendToPelicanTxt);
            FileInfo[] files = dirs.GetFiles("*.txt");

            for (var i = 0; i < files.Length; i++)
            {
                bool isUploadSuccess = true;
                string ftpUriUploadShunt = "";
                if (files[i].Name.EndsWith("R.txt"))            //這裡是來回件上傳
                {
                    ftpUriUploadShunt = ftpUriUpload + ftpUriUploadReturn;
                }
                else if (files[i].Name.EndsWith("Q.txt"))
                {
                    ftpUriUploadShunt = ftpUriUpload + ftpUriUploadRoundTripReturn;
                }
                else
                {
                    ftpUriUploadShunt = ftpUriUpload;
                }

                if (!Directory.Exists(pathSendToPelicanTxtUploaded))
                {
                    Directory.CreateDirectory(pathSendToPelicanTxtUploaded);
                }

                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using (StreamReader sourceStream = new StreamReader(pathSendToPelicanTxt + files[i].Name, Encoding.GetEncoding("big5")))
                {
                    data = Encoding.GetEncoding("big5").GetBytes(sourceStream.ReadToEnd().ToString());
                    WebClient wc = new WebClient();
                    wc.Credentials = new NetworkCredential(ftpAccountUpload, ftpPasswordUpload);
                    try
                    {
                        wc.UploadData(ftpUriUploadShunt + files[i].Name, data);     //成功的話移開到別的資料夾
                    }
                    catch (Exception e)
                    {
                        isSuccess = false;
                        isUploadSuccess = false;
                        ExceptionRecord(4, e);
                    }
                }

                if (isUploadSuccess)
                {
                    try
                    {
                        File.Move(pathSendToPelicanTxt + files[i].Name, pathSendToPelicanTxtUploaded + files[i].Name);
                    }
                    catch (Exception e)
                    {
                        isSuccess = false;
                        ExceptionRecord(4, e);
                    }
                }
            }

            WriteDbLog(4, isSuccess, true, insertId);
        }

        private static void FtpCatchFromPelican()
        {
            bool isSuccess = true;
            int insertId = WriteDbLog(6, false, false);

            string lastSuccessTime;     //格式yyyyMM_dd
            string cmdText = @"select start_time from Pelican_schedule_log with(nolock) where step = 6 and is_success = 1";
            using (SqlCommand cmd = new SqlCommand(cmdText))
            {
                try
                {
                    cmd.Connection = new SqlConnection(AWSConnectionString);
                    cmd.Connection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            lastSuccessTime = DateTime.Now.ToString("yyyyMM_dd");
                        }
                        else
                        {
                            while (reader.Read())
                            {
                                lastSuccessTime = ((DateTime)reader["start_time"]).ToString("yyyyMM_dd");
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    isSuccess = false;
                    ExceptionRecord(6, e);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            //以上這段也要依照回覆檔產生形式決定怎麼抓檔案
            //是否舊檔覆蓋新檔(重複寫入資料庫)，是否完整抓取資料庫，是否會產生空檔案，已完成..吧?

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            WebClient wc = new WebClient();
            wc.Credentials = new NetworkCredential(ftpAccountDownload, ftpPasswordDownload);
            string content = "";
            string fileName = string.Format(@"8362991401_{0}.txt", DateTime.Now.ToString("yyyyMM_dd"));
            try
            {
                byte[] file = wc.DownloadData(ftpUriDownload + fileName);
                content = Encoding.GetEncoding("big5").GetString(file);
                //
                if (!File.Exists(ftpReturnPath + fileName))
                {
                    var newFile = File.Create(ftpReturnPath + fileName);
                    newFile.Close();
                }
                using (StreamWriter sw = new StreamWriter(ftpReturnPath + fileName, false, Encoding.GetEncoding("big5")))       //第二個參數決定是否覆蓋
                {
                    sw.Write(content);
                }
            }
            catch (Exception e)
            {
                isSuccess = false;
                ExceptionRecord(6, e);
            }

            //以下抓取逆物流ftp

            string fileNameReturn = string.Format(@"R8362991401_{0}.txt", DateTime.Now.ToString("yyyyMM_dd"));
            try
            {
                byte[] file = wc.DownloadData(ftpUriDownload + fileNameReturn);
                content = Encoding.GetEncoding("big5").GetString(file);
                //
                if (!File.Exists(ftpReturnPath + fileNameReturn))
                {
                    var newFile = File.Create(ftpReturnPath + fileNameReturn);
                    newFile.Close();
                }
                using (StreamWriter sw = new StreamWriter(ftpReturnPath + fileNameReturn, false, Encoding.GetEncoding("big5")))       //第二個參數決定是否覆蓋
                {
                    sw.Write(content);
                }
            }
            catch (Exception e)
            {
                isSuccess = false;
                ExceptionRecord(6, e);
            }

            WriteDbLog(6, isSuccess, true, insertId);
        }

        private static byte[] CatchByteArrayRangeByIndexRange(byte[] array, int start, int length)     //包含start
        {
            byte[] result = new byte[length];
            for (var i = 0; i < length; i++)
            {
                result[i] = array[start + i];
            }
            return result;
        }

        private static ScanItemAndArriveOptionEntity ReturnSA(string endCode, string statusCode, string statusCodeDetail, List<PelicanStatusEntity> statusEntities, bool isD)
        {
            IEnumerable<PelicanStatusEntity> statuPaired = statusEntities.Where(x => x.PelicanStatusCode == statusCodeDetail && ((x.IsDeliveryD && isD) || (x.IsDeliveryR && !isD)));
            if (statuPaired.Count() > 0)
            {
                PelicanStatusEntity status = statuPaired.FirstOrDefault();
                ScanItemAndArriveOptionEntity result = new ScanItemAndArriveOptionEntity { ScanItem = status.ScanItem, ArriveOption = status.ArriveOption };
                return result;
            }
            else
            {
                PelicanStatusEntity status = statusEntities.Where(x => x.PelicanStatusCode == statusCode && x.EndCode == endCode && ((x.IsDeliveryD && isD) || (x.IsDeliveryR && !isD))).FirstOrDefault();
                if (status == null)
                {
                    return new ScanItemAndArriveOptionEntity { ScanItem = "", ArriveOption = "" };
                }

                ScanItemAndArriveOptionEntity result = new ScanItemAndArriveOptionEntity { ScanItem = status.ScanItem, ArriveOption = status.ArriveOption };
                return result;
            }
        }

        private static void ExceptionRecord(int step, Exception e)
        {
            if (!Directory.Exists(exceptionLogFolder))
            {
                Directory.CreateDirectory(exceptionLogFolder);
            }
            if (!File.Exists(exceptionLogFolder + DateTime.Now.ToString("yyyyMMdd") + "_" + exceptionLogFileName))
            {
                var file = File.Create(exceptionLogFolder + DateTime.Now.ToString("yyyyMMdd") + "_" + exceptionLogFileName);
                file.Close();
            }
            File.AppendAllText(exceptionLogFolder + DateTime.Now.ToString("yyyyMMdd") + "_" + exceptionLogFileName, "Step: " + step + "   " + DateTime.Now.ToString() + "\r\n" + e.ToString() + "\r\n---------------------------------------------" + "\r\n");

            try
            {
                string cmdText = string.Format(@"update {0} set remark = '{1}' where id = {2}", logPelicanSchedule, e.ToString(), step);
                using (SqlCommand cmd = new SqlCommand(cmdText))
                {
                    cmd.Connection = new SqlConnection(AWSConnectionString);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private static bool CreateFtpFile(string result, int sheduleLogId, int fileType)            //type-->0:正物流，1:逆物流，2:來回件(去件)，3:來回件(回件)
        {
            bool isSuccess = true;
            if (result.Length > 0)
            {
                string fileName = pathSendToPelicanTxt + JFCustomerCode;
                switch (fileType)
                {
                    case 0:
                        fileName += "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".txt";
                        break;
                    case 1:
                        fileName += "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "R" + ".txt";
                        break;
                    case 2:
                        fileName += "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "P" + ".txt";
                        break;
                    case 3:
                        fileName += "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "Q" + ".txt";
                        break;
                }


                if (!File.Exists(fileName))
                {
                    var newFile = File.Create(fileName);
                    newFile.Close();
                }
                //File.AppendAllLines(fileName, resultList.ToArray());
                using (StreamWriter sw = new StreamWriter(fileName, false, Encoding.GetEncoding("big5")))
                {
                    sw.Write(result);
                }

                //以下將新增txt檔案檔名寫入schedulelog的對應欄位
                string DbColumnName = "";
                switch (fileType)
                {
                    case 0:
                        DbColumnName = "file_name_D";
                        break;
                    case 1:
                        DbColumnName = "file_name_R";
                        break;
                    case 2:
                        DbColumnName = "file_name_P_D";
                        break;
                    case 3:
                        DbColumnName = "file_name_P_R";
                        break;
                }

                string cmdUpdateFileText = string.Format(@"update {0} set {3} = '{1}' where id = {2}", logPelicanSchedule, fileName, sheduleLogId, DbColumnName);
                using (SqlCommand cmdUpdateFile = new SqlCommand(cmdUpdateFileText))
                {
                    try
                    {
                        cmdUpdateFile.Connection = new SqlConnection(AWSConnectionString);
                        cmdUpdateFile.Connection.Open();
                        cmdUpdateFile.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        isSuccess = false;
                        ExceptionRecord(2, e);
                    }
                    finally
                    {
                        cmdUpdateFile.Connection.Close();
                    }
                }
            }
            return isSuccess;
        }

        private static bool IsPelicanCheckNumber(string checkNumber)        //這段看起來怪怪的，不過好像沒在用
        {
            string cmdText = string.Format(@"select * from ");
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = new SqlConnection(AWSConnectionString);
                cmd.Connection.Open();

                cmd.Connection.Close();
            }
            return true;
        }

        private static void Alertor(int thisTimeNeedCheckNumberCountTypeD, int thisTimeNeedCheckNumberCountTypeR)
        {
            bool isSuccess = true;
            int insertId = WriteDbLog(7, false, false);
            ResidueCheckNumber ResidueTypeD = ResidueCheckNumberCountChecker("D");
            ResidueCheckNumber ResidueTypeR = ResidueCheckNumberCountChecker("R");
            int countResidueTypeD = ResidueTypeD.ResidueCheckNumberCount;
            int countResidueTypeR = ResidueTypeR.ResidueCheckNumberCount;

            if (countResidueTypeD != -1 && countResidueTypeD - thisTimeNeedCheckNumberCountTypeD < alertIfCheckNumberCountLessThanTypeD)
            {
                if (SendMail(true, countResidueTypeD))
                {
                    UpdateAlertTime(ResidueTypeD.Ids);
                };
            }

            if (countResidueTypeR != -1 && countResidueTypeR - thisTimeNeedCheckNumberCountTypeR < alertIfCheckNumberCountLessThanTypeR)
            {
                if (SendMail(false, countResidueTypeR))
                {
                    UpdateAlertTime(ResidueTypeR.Ids);
                };
            }

            WriteDbLog(7, isSuccess, true, insertId);
        }

        private static ResidueCheckNumber ResidueCheckNumberCountChecker(string DorR)
        {
            string cmdTextForSender = string.Format("select * from {0} with(nolock) where active_flag = 1 and delivery_type = '{2}' and company_id = {1} and priority in (select priority from {0} where active_flag = 1 and delivery_type = '{2}' and company_id = {1} and number_end>=number_next)", intermodalTransportationCheckNumber, companyIdPelican, DorR);
            int count = 0;
            bool isNeedToAlert = false;
            string ids = "";

            using (SqlCommand cmdChooseSender = new SqlCommand(cmdTextForSender))
            {
                try
                {
                    using (cmdChooseSender.Connection = new SqlConnection(AWSConnectionString))
                    {
                        cmdChooseSender.Connection.Open();
                        using (SqlDataReader sr = cmdChooseSender.ExecuteReader())
                        {
                            while (sr.Read())
                            {
                                count += (int)((long.Parse(sr["number_end"].ToString()) - long.Parse(sr["number_next"].ToString())) / 10);
                                isNeedToAlert = isNeedToAlert || sr["alert_not_enough_time"].ToString() == "";
                                ids += sr["id"].ToString() + ",";
                            }
                        };

                    };
                }
                catch (Exception ex)
                {
                    ExceptionRecord(999, ex);
                }
                finally
                {
                    cmdChooseSender.Connection.Close();
                }
            }

            ResidueCheckNumber result = new ResidueCheckNumber()
            {
                Ids = ids.Substring(0, ids.Length - 1),
                ResidueCheckNumberCount = isNeedToAlert ? count : -1
            };

            return result;
        }

        private static bool SendMail(bool isDelivery, int residueCheckNumberCount)       //只允許gmail寄信
        {
            string sendAccount = "";
            string sendPassword = "";
            List<string> receiveAccounts = new List<string>();
            DateTime now = DateTime.Now;

            string cmdTextForSender = string.Format("select TOP 1 * from PelicanEmail with(nolock) where is_sender = 1 and active_date < '{0}'  and '{0}' < unable_date and mail_password is not null", now.ToString("yyyy-MM-dd HH:mm:ss"));
            using (SqlCommand cmdChooseSender = new SqlCommand(cmdTextForSender))
            {
                try
                {
                    using (cmdChooseSender.Connection = new SqlConnection(AWSConnectionString))
                    {
                        cmdChooseSender.Connection.Open();
                        using (SqlDataReader sr = cmdChooseSender.ExecuteReader())
                        {
                            while (sr.Read())
                            {
                                sendAccount = sr["mail_account"].ToString();
                                sendPassword = sr["mail_password"].ToString();
                            }
                        };

                    };
                }
                catch (Exception ex)
                {
                    ExceptionRecord(999, ex);
                    return false;
                }
                finally
                {
                    cmdChooseSender.Connection.Close();
                }
            }

            string cmdTextForReceiver = string.Format("select * from PelicanEmail with(nolock) where is_sender = 0 and active_date < '{0}' and '{0}' < unable_date", now.ToString("yyyy-MM-dd HH:mm:ss"));
            using (SqlCommand cmdChooseReceivers = new SqlCommand(cmdTextForReceiver))
            {
                try
                {
                    using (cmdChooseReceivers.Connection = new SqlConnection(AWSConnectionString))
                    {
                        cmdChooseReceivers.Connection.Open();
                        using (SqlDataReader sr = cmdChooseReceivers.ExecuteReader())
                        {
                            while (sr.Read())
                            {
                                receiveAccounts.Add(sr["mail_account"].ToString());
                            }
                        };
                        cmdChooseReceivers.Connection.Close();
                    };
                }
                catch (Exception ex)
                {
                    ExceptionRecord(999, ex);
                    return false;
                }
                finally
                {
                    cmdChooseReceivers.Connection.Close();
                }
            }


            try
            {
                if (receiveAccounts.Count() > 0)
                {
                    string smtpAddress = "smtp.gmail.com";
                    //設定Port
                    int portNumber = 587;
                    bool enableSSL = true;
                    //填入寄送方email和密碼
                    string emailFrom = sendAccount;
                    string password = sendPassword;
                    //主旨
                    string subject = string.Format("宅配通{0}物流貨號剩餘不足{1}組", isDelivery ? "正" : "逆", isDelivery ? alertIfCheckNumberCountLessThanTypeD.ToString() : alertIfCheckNumberCountLessThanTypeR.ToString());
                    //內容
                    string body = string.Format("{0} 宅配通{1}物流貨號剩餘{2}組", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), isDelivery ? "正" : "逆", residueCheckNumberCount.ToString());

                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress(emailFrom);
                        for (var r = 0; r < receiveAccounts.Count(); r++)
                        {
                            mail.To.Add(receiveAccounts[r]);
                        }
                        mail.Subject = subject;
                        mail.Body = body;
                        // 若你的內容是HTML格式，則為True
                        mail.IsBodyHtml = false;

                        using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                        {
                            smtp.Credentials = new NetworkCredential(emailFrom, password);
                            smtp.EnableSsl = enableSSL;
                            smtp.Send(mail);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionRecord(999, ex);
                return false;
            }

            return true;
        }

        private static void UpdateAlertTime(string ids)
        {
            string cmdText = string.Format(@"update {0} set alert_not_enough_time = '{1}' where id in ({2})",intermodalTransportationCheckNumber, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),ids);
            using (SqlCommand cmd = new SqlCommand(cmdText))
            {
                try
                {
                    using (cmd.Connection = new SqlConnection(AWSConnectionString))
                    {
                        cmd.Connection.Open();
                        cmd.ExecuteReader();
                    }
                }
                catch(Exception ex)
                {

                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
        }

        public class DeliveryRequestEntity
        {
            public string JFRequestId { get; set; }
            public string JFCheckNumber { get; set; }
            public string PelicanCheckNumber { get; set; }
            public string OrderNumber { get; set; }
            public string Pieces { get; set; }
            public string SendContact { get; set; }
            public string SendTel { get; set; }
            public string SendCity { get; set; }
            public string SendArea { get; set; }
            public string SendAddress { get; set; }
            public string SendZip3 { get; set; }
            public string ReceiveContact { get; set; }
            public string ReceiveTel { get; set; }
            public string ReceiveCity { get; set; }
            public string ReceiveArea { get; set; }
            public string ReceiveAddress { get; set; }
            public string ReceiveZip3 { get; set; }
            public string ArriveAssignDate { get; set; }
            public string AssignedTimePeriod { get; set; }
            public string CollectionMoney { get; set; }
            public string DeliveryType { get; set; }
            public string Remark { get; set; }
            public string JfCustomerCode { get; set; }
            public string PelicanReceiveZip5 { get; set; }
            public string PelicanReceiveArea { get; set; }
            public string RoundTrip { get; set; }
            public string DrRequestId { get; set; }
            public string RoundTripCheckNumber { get; set; }
            public bool IsNeedToInsertTable { get; set; }
        }

        public class PelicanStatusEntity
        {
            public string PelicanStatusCode { get; set; }
            public string ScanItem { get; set; }
            public string ArriveOption { get; set; }
            public bool IsDeliveryD { get; set; }
            public bool IsDeliveryR { get; set; }
            public string EndCode { get; set; }
        }

        public class PelicanCreateReuturnEntity
        {
            public string PZip5 { get; set; }
            public string Area { get; set; }
            public bool IsSuccess { get; set; }
        }

        public class PostZipEntity
        {
            public string PostCity { get; set; }
            public string PostArea { get; set; }
            public string Zip { get; set; }
        }

        public class JfCustomerCodeAndPelicanCheckNumber
        {
            public string JfCustomerCode { get; set; }
            public string PelicanCheckNumber { get; set; }
            public string NumberEnd { get; set; }
        }

        public class PostFormatEntity
        {
            public int Id { get; set; }
            public bool IsNecessary { get; set; }
            public string ColumnName { get; set; }
            public bool IsNumberOnly { get; set; }
            public int StartLocation { get; set; }
            public int EndLocation { get; set; }
            public string Remark { get; set; }
        }

        public class TtDeliveryScanLogEntity
        {
            public string DriverCode { get; set; }
            public string CheckNumber { get; set; }
            public string ScanItem { get; set; }
            public string ScanDate { get; set; }
            public string AreaArriveCode { get; set; }
            public string ArriveOption { get; set; }
            public string CDate { get; set; }

            //以下是為了回覆檔log
            public string ScanLogId { get; set; }
            public string ReturnFilename { get; set; }
            public string AccountCode { get; set; }
            public string PelicanCheckNumber { get; set; }
            public string ReceiveZip3 { get; set; }
            public string InformationCreateDate { get; set; }
            public string ReceiveDate { get; set; }
            public string ReturnFileCreateTime { get; set; }
            public string EndCode { get; set; }
            public string StatusCode { get; set; }
            public string ScanStationCode { get; set; }
            public string ScanStationShortname { get; set; }
            public string StatusChinese { get; set; }
            public string ReceiveCode { get; set; }
            public string AbnormalStatusCode { get; set; }
            public string AbnormalStatusChinese { get; set; }
            public bool IsD { get; set; }       //是否為正物流
        }

        public class ScanItemAndArriveOptionEntity
        {
            public string ScanItem { get; set; }
            public string ArriveOption { get; set; }
        }

        public class ResidueCheckNumber
        {
            public string Ids { get; set; }
            public int ResidueCheckNumberCount { get; set; }
        }

        static void Main(string[] args)
        {
            //ReadReturnFile();
            Primary();
            Console.WriteLine("執行完畢");
        }
    }




}
